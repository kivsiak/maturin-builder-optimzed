FROM rust:slim

ENV XWIN_ACCEPT_LICENSE=true
ENV XDG_CACHE_HOME=/root/.cache

RUN rustup target add x86_64-pc-windows-msvc

RUN cargo install maturin

RUN apt update && \
    apt install -y \
#    libssl-dev \
#    libclang-dev \
    llvm && \
    rm -rf /var/lib/apt/lists/*


RUN cargo install xwin && \
    xwin  --cache-dir /root/.cache/cargo-xwin/xwin download && \
    xwin  --cache-dir /root/.cache/cargo-xwin/xwin unpack && \
    xwin  --cache-dir /root/.cache/cargo-xwin/xwin splat && \
    mv /root/.cache/cargo-xwin/xwin/splat/* /root/.cache/cargo-xwin/xwin/ && \
    rm -rf /root/.cache/cargo-xwin/xwin/dl && \
    rm -rf  /root/.cache/cargo-xwin/xwin/splat && \
    rm -rf /root/.cache/cargo-xwin/xwin/unpack && \
    cargo uninstall xwin

COPY DONE /root/.cache/cargo-xwin/xwin/

WORKDIR /opt/
