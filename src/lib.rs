use pyo3::prelude::*;


#[pyfunction]
fn test(a: i64, b: i64) -> i64 {
    return a + b;
}

#[pymodule]
fn mylib(_py: Python, m: &PyModule) -> PyResult<()> {
    m.add_function(wrap_pyfunction!(test, m)?)?;

    Ok(())
}